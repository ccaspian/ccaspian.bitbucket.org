
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

//trije pacienti:

var pacienti = [
    {
        "ime": "Robert",
        "priimek": "Koren",
        "datumr": "1986-03-02",
        "sistol_b": ["120", "117","112", "101","100", "88","80"],
        "diastol_b": ["80","72","70","66","64","62","60"],
        "hr":["82","79","76","71","66","62","56"],
        "spO2":["99","99","95","95","96","99","99"],
    },
    {
        "ime": "Maja",
        "priimek": "Sever",
        "datumr": "1993-01-04",
        "sistol_b": ["110","120","123","117","98","90","85",],
        "diastol_b": ["90","92","93","90","83","79","72"],
        "hr":["61","59","67","69","72","76","57"],
        "spO2":["98","98","98","98","98","98","98"],
    },
    {
        "ime": "Niko",
        "priimek": "Radonja",
        "datumr": "1996-09-10",
        "sistol_b": ["100","101","101","102","103","102","104",],
        "diastol_b": ["70","72","72","71","73","73","72"],
        "hr":["65","61","68","69","67","66","68"],
        "spO2":["98","97","97","96","97","98","99"],
    }
];

var dates = [ 
    "2013-01-01T12:00", 
    "2013-01-06T12:00",
    "2013-01-11T12:00",
    "2013-01-16T12:00",
    "2013-01-21T12:00",
    "2013-01-26T12:00",
    "2013-01-31T12:00" 
];


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */

function generirajPodatkeMaster(){
    $("#kreirajSporocilo").empty();
    for(var i = 1;i<=pacienti.length;i++){
        generirajPodatke(i);
    }
}
function generirajPodatke(stPacienta) {
    sessionId = getSessionId();
  
    $.ajaxSetup({
        headers: {"Ehr-Session": sessionId}
    });
    $.ajax({
          url: baseUrl + "/ehr",
          type: 'POST',
          success: function (data) {
              var ehrId = data.ehrId;
              var partyData = {
                  firstNames: pacienti[stPacienta-1].ime,
                  lastNames: pacienti[stPacienta-1].priimek,
                  dateOfBirth: pacienti[stPacienta-1].datumr,
                  partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
              };
              $.ajax({
                  url: baseUrl + "/demographics/party",
                  type: 'POST',
                  contentType: 'application/json',
                  data: JSON.stringify(partyData),
                  success: function (party) {
                      if (party.action == 'CREATE') {
                          $('#preberiObstojeciEHR').append($('<option>', {
                              value: data.ehrId,
                              text: pacienti[stPacienta-1].ime
                          }));
                          $('#preberiEhrIdZaVitalneZnake').append($('<option>', {
                              value: data.ehrId,
                              text: pacienti[stPacienta-1].ime
                          }));
                      }
                      //console.log("generirajPodatke() --> success!");
                      $("#kreirajSporocilo").append("<br><span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" +
                      data.ehrId + "' ("+pacienti[stPacienta-1].ime+" "+pacienti[stPacienta-1].priimek+").</span>");
                  },
                  error: function(err) {
                      $("#kreirajSporocilo").html("<span class='obvestilo label " +
                      "label-danger fade-in'>Napaka '" +
                      JSON.parse(err.responseText).userMessage + "'!");
                      //console.log("generirajPodatke() --> faliure: \n"+JSON.parse(err.responseText).userMessage);
                  }
              });
              for(var i = 0;i<dates.length;i++){
                  generirajVitalneZnakePacienta(stPacienta,i,data.ehrId);
              }
              appendOnlyOnePerUser(stPacienta-1,data.ehrId);
          }
      });
}
function generirajVitalneZnakePacienta(stPacienta,day,ehrId){
    sessionId = getSessionId();

    $.ajaxSetup({
        headers: {"Ehr-Session": sessionId}
    });
    var podatki = {
        // Struktura predloge je na voljo na naslednjem spletnem naslovu:
        // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
        "ctx/language": "en",
        "ctx/territory": "SI",
        "ctx/time": dates[day],
        "vital_signs/blood_pressure/any_event/systolic": pacienti[stPacienta-1].sistol_b[day],
        "vital_signs/blood_pressure/any_event/diastolic": pacienti[stPacienta-1].diastol_b[day],
        "vital_signs/indirect_oximetry:0/spo2|numerator": pacienti[stPacienta-1].spO2[day],
        "vital_signs/pulse:0/any_event:0/rate|magnitude": pacienti[stPacienta-1].hr[day]

    };
    //console.log(pacienti[stPacienta-1].ehr);
    var parametriZahteve = {
        ehrId: ehrId,
        templateId: 'Vital Signs',
        format: 'FLAT',
        committer: 'samostojno'
    };
    $.ajax({
        url: baseUrl + "/composition?" + $.param(parametriZahteve),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(podatki),
        success: function (res) {
            //console.log("generirajVitalneZnakePacienta() --> success!");
        },
        error: function(err) {
            //console.log("generirajVitalneZnakePacienta() --> faliure: \n"+JSON.parse(err.responseText).userMessage);
        }
    });
}
function appendOnlyOnePerUser(i,ehr){
    var preberi = $("#preberiObstojeciVitalniZnak");
    preberi.append($('<option>',{
        value: ehr+"|"+dates[1]+"|"+pacienti[i].sistol_b[1]+"|"+pacienti[i].diastol_b[1]+"|"+pacienti[i].spO2[1]+"|"+pacienti[i].hr[1],
        text: pacienti[i].ime
    }))
}
/**
 * Kreiranje EHR za bolnika:
 */
function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
    var datumRojstva = $("#kreirajDatumRojstva").val() + "T00:00:00.000Z";

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                            "label label-success fade-in'>Uspešno kreiran EHR '" +
                            ehrId + "'.</span>");
                            $("#preberiEHRid").val(ehrId);
                            
                            $('#preberiObstojeciEHR').append($('<option>', {
                                value: ehrId,
                                text: ime
                            }));
                            $('#preberiEhrIdZaVitalneZnake').append($('<option>', {
                                value: ehrId,
                                text: ime
                            }));
                            $('#preberiObstojeciVitalniZnak').append($('<option>', {
                                value: ehrId,
                                text: ime
                            }));
                        }
                        
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}

/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preberiEHRodBolnika() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Uporabnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}

/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom in merilec).
 */
function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
	var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
    var nasicenostKrviSKisikom = $("#dodajVitalnoNasicenostKrviSKisikom").val();
    var utripSrca = $("#dodajVitalnoSrcniUtrip").val();
    $("#dodajVitalnoDatumInUra").val("");
    $("#dodajVitalnoKrvniTlakSistolicni").val("");
    $("#dodajVitalnoKrvniTlakDiastolicni").val("");
    $("#dodajVitalnoNasicenostKrviSKisikom").val("");
    $("#dodajVitalnoSrcniUtrip").val("");

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
            "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom,
            "vital_signs/pulse:0/any_event:0/rate|magnitude": utripSrca

		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: 'samostojno'
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}

/**
 * Pridobivanje ustreznih zahtevanih podatkov
 * kot so sistolični ali diastolični krvni tlak, pritisk in nasičenost
 * krvi s kisikom
 */
function preberiMeritveVitalnihZnakov() {
    sessionId = getSessionId();
    
    $('#chart').empty();

	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
    var tip = $("#preberiTipZaVitalneZnake").val();

	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
                "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
                " " + party.lastNames + "'</b>.</span><br/><br/>");

				if (tip == "Sistolični krvni tlak") {
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
                                var formData = [];
						        for (var i in res) {
                                    var item = res[i];
                                    formData.push({
                                        "time":item.time,
                                        "value":item.systolic
                                    });
                                }
                                console.log(formData);
                                generirajGraf(formData,'time');
                            
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                                "<span class='obvestilo label label-warning fade-in'>" +
                                "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                            JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				} else if (tip == "Diastolični krvni tlak") {
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
                                if (res.length > 0) {
                                    var formData = [];
                                    for (var i in res) {
                                        var item = res[i];
                                        formData.push({
                                            "time":item.time,
                                            "value":item.diastolic
                                        });
                                    }
                                    console.log(formData);
                                    generirajGraf(formData,'time');  
                                }  else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                                "<span class='obvestilo label label-warning fade-in'>" +
                                "Ni podatkov!</span>");
					    	    }
                            }
                        },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                            JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				} else if (tip == "Srčni utrip") {
					$.ajax({
                        url: baseUrl + "/view/" + ehrId + "/" + "pulse",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
                                if (res.length > 0) {
                                    var formData = [];
                                    for (var i in res) {
                                        var item = res[i];
                                        formData.push(
                                            {
                                                "time":item.time,
                                                "value":item.pulse
                                            }
                                        );
                                    }
                                    console.log(formData);
                                    generirajGraf(formData,'time');  
                                }  else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                                "<span class='obvestilo label label-warning fade-in'>" +
                                "Ni podatkov!</span>");
					    	    }
                        },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				} else if (tip == "Nasičenost krvi s kisikom") {
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "spO2",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
                                if (res.length > 0) {
                                    var formData = [];
                                    for (var i in res) {
                                        var item = res[i];
                                        formData.push({
                                            "time":item.time,
                                            "value":item.spO2
                                        });
                                    }
                                    console.log(formData);
                                    generirajGraf(formData,'time');  
                                }  else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                                "<span class='obvestilo label label-warning fade-in'>" +
                                "Ni podatkov!</span>");
					    	    }
                            }
                        },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				}
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}
/**
 * S pomočjo @method generirajGraf() narišemo graf podatkov, ki jih je uporabnik zahteval
 * @param formData to so podatki, ki jih potrebujemo da narisemo graf
 * @param x kaj se izpiše na spodnejm delu grafa
 */
function generirajGraf(formData,x){

    new Morris.Line({
        // ID of the element in which to draw the chart.
        element: 'chart',
        // Chart data records -- each entry in this array corresponds to a point on
        // the chart.
        data: formData,
        
          // The name of the data record attribute that contains x-values.
        xkey: 'time',
          // A list of names of data record attributes that contain y-values.
        ykeys: ['value'],
          // Labels for the ykeys -- will be displayed when you hover over the
          // chart.
        labels: ['Value']
      });
}

var map;
var infowindow;
var mesta = {
    "ljubljana" : { 
        lat:46.056946, 
        lng:14.505751
    },
    "kamnik" : {
        lat:46.2221964, 
        lng:14.6072967
    },
    "maribor" : {
        lat:46.5546503, 
        lng:15.6458811
    },
    "koper" : {
        lat:45.54805899, 
        lng:13.73018769
    }
};
function prikaziZemljevid(){
    window.location.href = 'maps.html' + '#' + $("#preberiMesto").val();
}
function initMap() {
    var izbrano = mesta[window.location.hash.substring(1)];
    map = new google.maps.Map(document.getElementById('map'), {
        center: izbrano,
        zoom: 14
    });
    infowindow = new google.maps.InfoWindow();
    var service = new google.maps.places.PlacesService(map);
    service.nearbySearch({
        location: izbrano,
        radius: 5000,
        type: ['restaurant']
    }, callback);
}
function callback(results, status) {
    if (status === google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
            createMarker(results[i]);
        }
    }
}
function createMarker(place) {
    var placeLoc = place.geometry.location;
    var marker = new google.maps.Marker({
        map: map,
        position: place.geometry.location
    });
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(place.name);
        infowindow.open(map, this);
    });
}

$(document).ready(function() {
  
    /**
     * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
     * ko uporabnik izbere vrednost iz padajočega menuja
     * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
     */
      $('#preberiObstojeciEHR').change(function() {
          $("#preberiSporocilo").html("");
          $("#preberiEHRid").val($(this).val());
      });
  
    /**
     * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
     * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
     * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
     * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
     */
      $('#preberiObstojeciVitalniZnak').change(function() {
          $("#dodajMeritveVitalnihZnakovSporocilo").html("");
          var podatki = $(this).val().split("|");
          $("#dodajVitalnoEHR").val(podatki[0]);
          $("#dodajVitalnoDatumInUra").val(podatki[1]);
          $("#dodajVitalnoKrvniTlakSistolicni").val(podatki[2]);
          $("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[3]);
          $("#dodajVitalnoNasicenostKrviSKisikom").val(podatki[4]);
          $("#dodajVitalnoSrcniUtrip").val(podatki[5]);
      });
  
    /**
     * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
     * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
     * (npr. Ata Smrk, Pujsa Pepa)
     */
      $('#preberiEhrIdZaVitalneZnake').change(function() {
          $("#preberiMeritveVitalnihZnakovSporocilo").html("");
          $("#rezultatMeritveVitalnihZnakov").html("");
          $("#meritveVitalnihZnakovEHRid").val($(this).val());
      });
  
  });